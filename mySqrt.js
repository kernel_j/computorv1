// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   mySqrt.js                                          :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: jwong <marvin@42.fr>                       +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2017/04/14 14:18:34 by jwong             #+#    #+#             //
//   Updated: 2017/04/14 16:53:18 by jwong            ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

"use strict"

let mySqrt = function() {
    this.complex = false;
};

mySqrt.prototype.divideStr = function(number, index, whole) {
    let str;

    if (whole === false)
        str = number.substr(0, (index + 8))
    else
        str = number.substr(0, (index - 1));
    if (this.complex === true)
        str += 'i';
    return (str);
}

mySqrt.prototype.digits = function(num) {
	let number = num.toString();
    let index = number.indexOf('.');

    if (index === -1) {
        number += '.0';
        index = number.indexOf('.');
    }
    index += 1;
    for (var i = index; i < 8; i++) {
        if (number[i] !== '0') {
            return (this.divideStr(number, index, false));
        }
    }
    return (this.divideStr(number, index, true));
};

mySqrt.prototype.abs = function(num) {
    if (num < 0)
        num *= -1;
    return (num);
};

mySqrt.prototype.sqrt = function(num) {
	if (num === undefined)
		return ;
    num = Number(num);
	if (num != 0) {
        if (num < 0) {
            num *= -1;
            this.complex = true;
        }
		let x1 = num / 2.0;
		let x2 = (x1 + (num / x1)) / 2.0;
	
        while(this.abs(x1 - x2) >= 0.0001) {
			x1 = x2;
			x2 = (x1 + (num / x1)) / 2;
		}
		return (this.digits(x2));
	}
    else if (num == 0)
        return (0);
};

module.exports = mySqrt;
