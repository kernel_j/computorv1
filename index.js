// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   index.js                                           :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: jwong <marvin@42.fr>                       +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2017/04/06 15:10:10 by jwong             #+#    #+#             //
//   Updated: 2017/04/20 15:16:23 by jwong            ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

"use strict"

const readline = require('readline');
const COMPUTOR = require('./computorV1');
const MYSQRT = require('./mySqrt');

let rl = readline.createInterface({
	input: process.stdin
});

rl.on('line', (line) => {
	rl.close();
	let computor = new COMPUTOR();

	computor.solve(line).then((res) => {return (res)});
});
/*
let str = '-5 * X^0 + 4 * X^0 - 9.3 * X^1 = 1 * X^0';
let str1 = '42 * X^0 = 42 * X^0 + 5 * X^0';

let computor = new COMPUTOR();

console.log('input: ' + str1);
computor.solve(str1).then((res) => {console.log(res)});
*/
/*let mySqrt = new MYSQRT();

let answer = mySqrt.sqrt(-8);
console.log(answer);*/
