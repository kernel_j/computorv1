"use strict"

const FS = require('fs');

let logger = function() {
    this.now = new Date().toISOString();
};

logger.prototype.log = function(errMsg) {
    let msg = this.now + '\t' + errMsg + '\n';

    FS.open('errors.log', 'a+', (error, fd) => {
        if (error)
            return ;
        FS.write(fd, msg, (err) => {
            if (err)
                console.error("Error: could not write to file");
        });
        FS.close(fd);
    });

};

module.exports = logger;
