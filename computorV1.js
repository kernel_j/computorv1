// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   computorV1.js                                      :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: jwong <marvin@42.fr>                       +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2017/04/06 15:23:25 by jwong             #+#    #+#             //
//   Updated: 2017/06/15 19:19:25 by jwong            ###   ########.fr       //
//                                                                            //
// ************************************************************************** //
"use strict"

const MYSQRT = require('./mySqrt');
const LOGGER = require('./logger');

let logger = new LOGGER();

let computor = function() {
    this.degree;
    this.rhs;
    this.lhs;
    this.reduce;
    this.soln;
    this.discriminant;
    this.allSoln = false;
    this.regex = '[+-]? ?[-]?[0-9]*[.]?[0-9]* \\\* X\\\^';
};

computor.prototype.extract = function(input) {
    let index = input.indexOf('=');

    if (index === -1) {
        logger.log('Error: incorrect syntax');
        return ;
    }
    this.lhs = input.substr(0, index);
    this.rhs = input.substr((index + 1));
};

computor.prototype.getDegree = function(eq) {
    if (eq === null)
        return ;

    for (var i = 0; i < eq.length; i++) {
        if (eq[i] === 'X^0' && this.degree <= 0)
            this.degree = 0;
        else if(eq[i] === 'X^1' && this.degree <= 1)
            this.degree = 1;
        else if (eq[i] === 'X^2' && this.degree <= 2)
            this.degree = 2;
        else {
            let index = eq[i].indexOf('^');
            let degree = eq[i].substr(index + 1);

            if (degree > 2) {
                this.degree = degree;
                return ;
            }
        }
    }
};

computor.prototype.degree = function() {
    let reg = /X\^[-]?[0-9]*/g;
    let rhs = this.rhs.match(reg);
    let lhs = this.lhs.match(reg);

    this.degree = -1;
    this.getDegree(rhs);
    this.getDegree(lhs);
};

computor.prototype.joinConstants = function(eq) {
    let equation = '';

    if (eq === null)
        return (null);
    for (var i = 0; i < eq.length; i++) {
        let index = eq[i].indexOf('*');
        let num = eq[i].substr(0, index);

        equation += num;
    }
    return (equation);
};

computor.prototype.evaluate = function(lhs, rhs) {
    let leftEval;
    let rightEval;
    let solve;

    leftEval = this.joinConstants(lhs);
    rightEval = this.joinConstants(rhs);
    if (leftEval !== null)
        leftEval = eval(leftEval);
    else
        leftEval = 0;
    
    if (rightEval !== null) {
        rightEval = eval(rightEval);
        rightEval *= -1;
    }
    else
        rightEval = 0;
    solve = leftEval + rightEval;
    return (solve);
};

computor.prototype.reduceForm = function() {
    let rhs;
    let lhs;
    let soln;
    let reduce = '';

	for (var i = 0; i <= this.degree; i++) {
		let str = '';
		let reg = new RegExp(this.regex + i, 'g');
		lhs = this.lhs.match(reg);
		rhs = this.rhs.match(reg);
		soln = this.evaluate(lhs, rhs);
		if (soln < 0 && i !== 0) {
			soln *= -1;
			str = ' - ' + soln + ' * X^' + i;
		}
		else if (soln >= 0 && i !== 0)
			str = ' + ' + soln + ' * X^' + i;
		else if (i == 0)
			str = soln + ' * X^' + i;
		reduce += str; 
	}
    reduce += ' = 0';
    this.reduce = reduce;
};

computor.prototype.getConst = function(degree) {
    let a;
    let b;
    let c;

    for (var i = 0; i <= degree; i++) {
        let reg = new RegExp(this.regex + i, 'g');
        let constant = this.reduce.match(reg);
        let index;

        if (constant !== null) {
            index = constant[0].indexOf('*');
            if (i === 0 && index !== -1)
                c = constant[0].substr(0, index);
            else if (i === 1 && index !== -1) {
                b = constant[0].substr(0, index);
                b = b.replace(/\s/g, '');
            }
            else if (degree == 2 && i === 2 && index !== -1) {
                a = constant[0].substr(0, index);
                a = a.replace(/\s/g, '');
            }
        }
    }
    console.log(a);
    console.log(b);
    console.log(c);
    return ({a: a, b: b, c: c});
};

computor.prototype.complexNum = function(root, divisor, b) {
    let sub = root.substr(0, (root.length - 1));
    let tmp;
    let tmp2;
    let pos;
    let neg;

    tmp = eval('(-1 *' + b + ') /' + divisor);
    tmp2 = eval(sub + '/' + divisor);
    pos = tmp + ' + ' + tmp2 + 'i';
    neg = tmp + ' - ' + tmp2 + 'i';
    
    return ({pos: pos, neg: neg});
}

computor.prototype.quadratic = function(a, b, c) {
    let sqrt = new MYSQRT();
    let divisor;
    let div;
    let root;
    let pos;
    let neg;

    divisor = eval('2 * ' + a);
    div = eval('(' + b + '*' + b + ')' + '- (4 *' + a + '*' + c + ')');
    if (div == 0) {
        this.discriminant = 0;
        pos = eval('(-1 *' + b + ') /' +  divisor);
        return ({pos: pos});
    }
    else if (div > 0) {
        this.discriminant = 1;
        root = sqrt.sqrt(div);
        pos = eval('((-1 *' + b + ') + ' + root + ') /' + divisor);
        neg = eval('((-1 *' + b + ') - ' + root + ') /' + divisor);
        return ({pos: pos, neg: neg});
    }
    else if (div < 0) {
        this.discriminant = -1;
        root = sqrt.sqrt(div);
        return (this.complexNum(root, divisor, b));
    }
    else
        console.log('Error occurred');
};


computor.prototype.getSoln = function(a, b, c) {
    if (a === undefined) {
        if (c !== undefined && b !== undefined)
            this.soln = eval('(-1 *' + c + ') / ' + b);
        else if (c === undefined && b !== undefined)
            this.soln = 0;
        else if ((c === undefined && b === undefined)
                || (c !== undefined && b === undefined)) {
					if (c != undefined && c != 0)
						this.soln = 'There are no solutions.';
					else 
            			this.soln = 'All real numbers are solutions.';
            this.allSoln = true;
        }
    }
    else {
        if (a === undefined) {
            return ;
        }
        if (b === undefined || b == '-0' || b == '+0')
            b = 0;
        if (c === undefined || c == '-0' || c == '+0')
            c = 0;
        this.soln = this.quadratic(a, b, c);
    }
};

computor.prototype.calculate = function() {
	let obj;

    switch(this.degree) {
        case 0:
            obj = this.getConst(0);
            this.getSoln(obj.a, obj.b, obj.c);
            break;
        case 1:
            obj = this.getConst(1);
            this.getSoln(obj.a, obj.b, obj.c);
            break;
        case 2:
            obj = this.getConst(2);
            this.getSoln(obj.a, obj.b, obj.c);
            break;
    }
};

computor.prototype.printSummary = function() {
    if (this.allSoln) {
        console.log(this.soln);
        return ;
    }
    console.log('Reduced form: ' + this.reduce);
    console.log('Polynomial degree: ' + this.degree);
    if (this.degree < 2) {
        console.log('The solution is:');
        console.log(this.soln);
    }
    else if (this.degree == 2) {
        let str = 'Discriminant is strictly ';
        let str2 = ', the two solutions are:';

        if (this.discriminant === 1) {
            console.log(str + 'positive' + str2);
            console.log(this.soln.neg);
            console.log(this.soln.pos);
        }
        else if (this.discriminant === -1) {
            console.log(str + 'negative' + str2);
            console.log(this.soln.neg);
            console.log(this.soln.pos);
        }
        else if (this.discriminant === 0) {
            console.log('Discriminant is 0, the solution is:');
            console.log(this.soln.pos);
        }
        else
            console.log('This polynomial is unsolvable.');
    }
	else if (this.degree > 2)
		console.log('The polynomial degree is strictly greater than 2, I can\'t solve.');
};

computor.prototype.solve = function(input) {
    return new Promise((fulfill, reject) => {
        if (input === undefined || input.length === 0) {
            logger.log('Error: input is undefined in solve function');
            return ;
        }

        this.extract(input);
        this.degree();
        this.reduceForm();
        this.calculate();
        this.printSummary();
        fulfill('success');
    });
};

module.exports = computor;
