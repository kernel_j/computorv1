# README #

This program solves polynomials with degrees of 0, 1, and 2.
It takes a polynomial and returns: its reduced form, the degree,
and the solution(s) if it is solvable.

## Usage

Install dependencies with the command below

```js
npm install

```

Run index.js with node and enter the polynomial

Polynomial syntax:
constant * X^degree +/- ... = constant * X^degree

```js
node index.js

4 * X^0 + 5 * X^1 = 5 * X^0

```
